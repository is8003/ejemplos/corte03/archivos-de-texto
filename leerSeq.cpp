// Reading and printing a sequential file
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main(void)
{ 
   ifstream textFile("clients.txt"); // textFile = clients.txt

   // exits program if file cannot be opened 
   if (textFile.fail()) {
      cerr << "File could not be opened" << endl;
   } 
   else { // read account, name and balance from file
      unsigned int account; // account number
      string name; // account name
      double balance; // account balance

      cout << setw(10) << "Account" << setw(13) << "Name" << setw(13) <<
	      "Balance" << endl;
      textFile >> account >> name >> balance;

      // while not end of file
      while (!textFile.eof()) { 
	      cout << setw(10) << account << setw(13) << name << setw(13) <<
		      fixed << setprecision(2) << balance << endl;
	      textFile >> account >> name >> balance;
      } 

      textFile.close(); // closes file   
   } 
} 
