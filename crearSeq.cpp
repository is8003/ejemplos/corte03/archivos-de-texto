// Creating a sequential file
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main(void)
{ 
   ofstream textFile("clients.txt"); // textFile = clients.txt

   // Exit program if unable to create file 
   if (textFile.fail()) {
      cerr << "File could not be opened" << endl;
   } 
   else { 
      cout << "Enter the account, name, and balance." << endl;
      cout << "Enter EOF to end input." << endl;
      cout << "? ";

      unsigned int account; // account number
      string name; // account name
      double balance; // account balance

      cin >> account >> name >> balance;

      // write account, name and balance into file
      while (!cin.eof()) { 
	      textFile << account << " " << name << " " << fixed
		      << setprecision(2) << balance << endl;
	      cout << "? ";
	      cin >> account >> name >> balance;
      } 
      
      textFile.close(); // closes file   
   } 
} 
